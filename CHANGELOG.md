# ChangeLog 
>please update so those that are not psychic know what you may have done to the module, thank you! ;) We use Semantic Versioning.
    
## Unreleased 00/00/0000

## Version 0.3.0 04/14/2017
[code] - added typescript loader to webpack
[refactor] - removed the need for `/dist`
[refactor] - changed js in `online-resume-backend` and `main` to typescript
[code] - created a bootstrap in `online-resume-backed`

## Version 0.2.0 04/13/2017
[code] - added an ORM `Sequelize` to hold resume content
[refactor] - changed JS to Typescript in all the app except `main` and `online-resume-backend`

## Version 0.1.1 04/07/2017
[refactor] - created a class for `mailgun` and fixed socket.io flow in `app.ts` 
[refactor] - added a new flow for messages and ResponseObject
[bug] - fixed mailgun flow and sending 
[refactor] - changed targeted email in `mailgun`

## Version 0.1.0 
[code] - initial backend seeded and configured for mailgun
[code] - mailgun added.
