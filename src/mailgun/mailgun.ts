import * as Mailgun from 'mailgun-js';
import {ResponseObject} from '../models/response-object.model';
let api_key = 'key-ed460dddc4f3256b186de8627bde2595'; // please don't copy
let domain = 'sandboxc99685dd5e9744f7b7622eb30d07e945.mailgun.org'; // please don't copy
let mailgun = new Mailgun({apiKey: api_key, domain: domain});

export class MailGunService {
	public mailgunSend(formData): Promise<ResponseObject> {
		 const data = {
			from: formData['email'],
			to: 'rdev1163@outlook.com',
			subject: 'Resume response from ' + formData['sender'] + ': ' + formData['email'],
			text: formData['text']
		};
		return new Promise((resolve, reject) => {
			mailgun.messages().send(data, function(err, res) {
				if (err) {
					reject({
						status: 'Error',
						message: 'Sending error: ' + err
					});
				} else {
					resolve({
						status: 'OK',
						message: res.message
					});
				}
			});
		});
	}
}