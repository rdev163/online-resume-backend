import * as http from 'http';
import * as express from 'express';
import * as socketio from 'socket.io';
import * as ResumeStoreManager from './orm/store-manager';
import {ResponseObject} from './models/response-object.model';
import {MailGunService} from './mailgun/mailgun';
import {dbConfig} from './orm/config';

export class Server {
	private app: express.Application;
	private server;
	private sockets: any[] = [];
	private IO: socketio;
	private rm: ResumeStoreManager.ResumeStoreManager;
	private mg: MailGunService;

	public static bootstrap(): Server {
		return new Server();
	}

	constructor(){
		this.app = express();
		this.config();
		this.endPoints();
	}

	private config(): void {
		console.log('listening on port Taxi Cab');
		this.IO = socketio(this.server);
		this.server = http.createServer(this.app);
		this.server.listen(1729);
		this.rm = new ResumeStoreManager.ResumeStoreManager(dbConfig);
		this.mg = new MailGunService();
	}

	private endPoints(): void {
		this.IO.on('connection', socket => {
			socket.on('resume.get', () => {
				this.rm.getResume().then(resume => {
					socket.emit('resume.get.response', resume);
				});
			});
			this.sockets.push(socket);
			socket.on('message', body => {
				let responsePromise = this.mg.mailgunSend(body);
				responsePromise.then((response: ResponseObject) => {
					socket.emit('message.response', response);
				});
			});
			socket.on('disconnect', () => {
				this.sockets = this.sockets.filter(s => {
					return s !== socket;
				});
			});
		});
	}
}