import * as sequelizeStatic from 'sequelize';
import {DBConfig} from './config';
import {ContactModel} from './table-models/attributes/contact.attributes';
import {EducationModel} from './table-models/attributes/education.attributes';
import {ExperienceModel} from './table-models/attributes/experience.attribute';
import {ResumeAttributes, ResumeModel} from './table-models/attributes/resume.attributes';
import {TechBoxModel} from './table-models/attributes/tech-box.attributes';
import {contactModel} from './table-models/contact.table-model';
import {educationModel} from './table-models/education.table-model'
import {experienceModel} from './table-models/experience.table-model';
import {resumeModel} from './table-models/resume.table-model';
import {techBoxModel} from './table-models/tech-box.table-model';

export interface StoreManager {
	getResume(): Promise<any>;
}

export class ResumeStoreManager implements StoreManager {
	public sequelize: sequelizeStatic.Sequelize;
	public Contact: ContactModel;
	public TechBox: TechBoxModel;
	public Education: EducationModel;
	public Experience: ExperienceModel;
	public Resume: ResumeModel;
	constructor(private config: DBConfig) {
		this.sequelize = new sequelizeStatic(this.config.database, this.config.username, this.config.password, {
			host: this.config.host,
			dialect: this.config.dialect,
			pool: {
				max: this.config.pool.max,
				min: this.config.pool.min,
				idle: this.config.pool.idle
			},
			storage: this.config.storage
		});
		this.Contact = contactModel(sequelizeStatic, this.sequelize);
		this.TechBox = techBoxModel(sequelizeStatic, this.sequelize);
		this.Education = educationModel(sequelizeStatic, this.sequelize);
		this.Experience = experienceModel(sequelizeStatic, this.sequelize);
		this.Resume = resumeModel(sequelizeStatic, this.sequelize);
		this.Resume.hasOne(this.Contact);
		this.Resume.hasOne(this.TechBox);
		this.Resume.hasOne(this.Education);
		this.Resume.hasOne(this.Experience);
		this.sequelize.sync().then(() => {
			this.Resume.find({
				where: {id: '5ebf9e9d-8ce2-49f8-afac-d02d586790c5'},
				include:[this.Contact]
			}).then(instance => {
				instance.contact
				console.log('instance', instance.contact);
			})
			// 	this.Resume.findAll({
		// 		where: { attribute: {id: 'b039e4b0-ce7c-479b-9f21-ef6952919d84'}}
		// 		// include: [this.Contact]
		// 	}).then(instance => {
		// 		console.log('instance', instance);
		// 	})
		})
	}

	getResume(): Promise<any> {
		return new Promise((resolve) => {
			this.Resume.findAll({
				include: [this.Education, this.TechBox, this.Experience, this.Contact]
			}).then((response: any) => {
				let resume: ResumeAttributes;
				console.log(response[2]);
				response = response[2];
				resume = {
					introduction: response.introduction,
					closure: response.closure,
					updated_at: response.updated_at,
					title: response.title,
					contact: response.contact.dataValues,
					experience: response.experience.dataValues,
					education: response.education.dataValues,
					techBox: response.techBox.dataValues,
				};
				resolve(resume);
			})
		});
	}
}