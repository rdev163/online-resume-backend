import * as Sequelize from 'sequelize';

export interface ContactAttributes {
	id?: string;
	phone?: string;
	email?: string;
	resume_id?: string;
}

export interface ContactInstance extends Sequelize.Instance<ContactAttributes>, ContactAttributes {
}

export interface ContactModel extends Sequelize.Model<ContactInstance, ContactAttributes> {}
