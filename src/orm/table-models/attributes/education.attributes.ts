import * as Sequelize from 'sequelize';

export interface EducationAttributes {
	id?: string;
	schoolName?: string;
	briefFocus?: string;
	studies?: string;
	resume_id?: string;
}

export interface EducationInstance extends Sequelize.Instance<EducationAttributes>, EducationAttributes {
}

export interface EducationModel extends Sequelize.Model<EducationInstance, EducationAttributes> {}
