import * as Sequelize from 'sequelize';

export interface ExperienceAttributes {
	id?: string;
	businessName: string;
	position: string;
	timeLength: string;
	briefDuties: string;
	resume_id: string;
}

export interface ExperienceInstance extends Sequelize.Instance<ExperienceAttributes>, ExperienceAttributes {
}

export interface ExperienceModel extends Sequelize.Model<ExperienceInstance, ExperienceAttributes> {}