import * as Sequelize from 'sequelize';
import {ContactAttributes} from './contact.attributes';
import {ExperienceAttributes} from './experience.attribute';
import {EducationAttributes} from './education.attributes';
import {TechBoxAttributes} from './tech-box.attributes';

export interface ResumeAttributes {
	id?: string;
	title: string;
	introduction: string;
	closure: string;
	contact?: ContactAttributes;
	experience?: ExperienceAttributes;
	education?: EducationAttributes;
	techBox?: TechBoxAttributes;
	updated_at?: string;
}

export interface ResumeInstance extends Sequelize.Instance<ResumeAttributes>, ResumeAttributes {
}

export interface ResumeModel extends Sequelize.Model<ResumeInstance, ResumeAttributes> {}