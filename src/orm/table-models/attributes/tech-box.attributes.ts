import * as Sequelize from 'sequelize';

export interface TechBoxAttributes {
	id?: string;
	content?: string;
	resume_id?: string;
}

export interface TechBoxInstance extends Sequelize.Instance<TechBoxAttributes>, TechBoxAttributes {
}

export interface TechBoxModel extends Sequelize.Model<TechBoxInstance, TechBoxAttributes> {}