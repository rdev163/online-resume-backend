import {ContactModel} from './attributes/contact.attributes';

export function contactModel(DataTypes, sequelize): ContactModel {
	return <ContactModel>sequelize.define('contact', {
		id: {
			type: DataTypes.UUID,
			primaryKey: true,
			defaultValue: DataTypes.UUIDV4
		},
		email: {
			type: DataTypes.STRING,
		},
		phone: {
			type: DataTypes.STRING,
		},
		updated_at: DataTypes.DATE,
		deleted_at: DataTypes.DATE
		}, {
		freezeTableName: true,
		paranoid: false,
		underscored: true
		});
	}
