export function educationModel(DataTypes, sequelize)  {
	 return sequelize.define('education', {
		id: {
			type: DataTypes.UUID,
			primaryKey: true,
			defaultValue: DataTypes.UUIDV4
		},
		schoolName: {
			type: DataTypes.STRING
		},
		briefFocus: {
			type: DataTypes.STRING
		},
		studies: {
			type: DataTypes.TEXT
		},
		updated_at: DataTypes.DATE,
		deleted_at: DataTypes.DATE
		}, {
		freezeTableName: true,
		paranoid: false,
		underscored: true
	});
}