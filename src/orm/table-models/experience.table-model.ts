export function experienceModel(DataTypes, sequelize) {
	return sequelize.define('experience', {
		id: {
			type: DataTypes.UUID,
			primaryKey: true,
			defaultValue: DataTypes.UUIDV4
		},
		businessName: {
			type: DataTypes.STRING,
		},
		position: {
			type: DataTypes.STRING,
		},
		timeLength: {
			type: DataTypes.TEXT,
		},
		briefDuties: {
			type: DataTypes.TEXT,
		},
		updated_at: DataTypes.DATE,
		deleted_at: DataTypes.DATE
		}, {
		freezeTableName: true,
		paranoid: false,
		underscored: true
	});
}