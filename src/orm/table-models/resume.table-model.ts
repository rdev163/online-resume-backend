export function resumeModel(DataTypes, sequelize) {
	return sequelize.define('resume', {
		id: {
			type: DataTypes.UUID,
			primaryKey: true,
			defaultValue: DataTypes.UUIDV4
		},
		title: {
			type: DataTypes.STRING,
		},
		introduction: {
			type: DataTypes.STRING,
		},
		closure: {
			type: DataTypes.STRING,
		},
		updated_at:  DataTypes.DATE,
		deleted_at: DataTypes.DATE
		}, {
		freezeTableName: true,
		paranoid: false,
		underscored: true
	});
}