export function techBoxModel(DataTypes, sequelize ) {
	return sequelize.define('techBox', {
		id: {
			type: DataTypes.UUID,
			primaryKey: true,
			defaultValue: DataTypes.UUIDV4
		},
		content: {
			type: DataTypes.TEXT
		},
		updated_at: DataTypes.DATE,
		deleted_at: DataTypes.DATE
		}, {
		freezeTableName: true,
		paranoid: false,
		underscored: true
	});
}